local library = require "plugin.rokomobi"
local notifications = require "plugin.notifications"

notifications.registerForPushNotifications()

local function successCallback()
    print("Success");
end

local function failureCallback( error )
    print(error);
end

library.init("2eGPtLF/3HYajB80ayEnd3KnRCpF94LYvj2gjzwTbjU=")
library.addEvent( {name = "CoronaEvent"}, successCallback, failureCallback)
library.setUser( {userName = "CoronaUser"}, successCallback, failureCallback)

local function notificationListener( event )
    print ("notificationListener. Event type: ", event.type)

    if event.type == "remoteRegistration" then
        -- This device has just been registered for push notifications.
        -- Store the Registration ID that was assigned to this application by Google or Apple.
        myRegistrationId = event.token
        print ("token: ", myRegistrationId)
        library.registerPush(event)
    end
    if ( event.type == "remote" ) then
        --handle the push notification
        print ("Push notification with text: ", event.alert)
        print ("Push custom part: ", event.custom)
    end
end
--The notification Runtime listener should be handled from within "main.lua"
Runtime:addEventListener( "notification", notificationListener )